package com.ko.springboot.demo.mycoolspringbootapp.rest;

import java.time.LocalDateTime;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FunRestController {

	// expose "/" endpoint that returns "Hello World"
	
	@GetMapping("/")
	public String sayHello() {
		return "Hello World from Spring Boot! Time on Server is " + LocalDateTime.now();
	}
	
	// expose another endpoint for "training"
	@GetMapping("/training")
	public String getTrainingAdvice() {
		return "Train your core!";
	}
	
	@GetMapping("/proverb")
	public String getProverb() {
		return "There is no shame in not knowing; the shame lies in not finding out.";
	}
}
